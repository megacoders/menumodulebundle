<?php

namespace Megacoders\MenuModuleBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Megacoders\MenuModuleBundle\Model\BreadcrumbMenuItem;
use Megacoders\MenuModuleBundle\Model\MenuItem;
use Megacoders\PageBundle\Controller\Module\BaseModuleController;
use Megacoders\PageBundle\Entity\Meta;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Manager\PageManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends BaseModuleController
{
    const CURRENT_PAGE_PARENT = 'CURRENT_PAGE_PARENT';

    const CURRENT_PAGE = 'CURRENT_PAGE';

    const OTHER_PAGE = 'OTHER_PAGE';

    const SELECTED_PAGES = 'SELECTED_PAGES';

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var PageManager $pageManager */
        $pageManager = $this->get('page.manager.page_manager');

        $root = $this->getModuleParameter('root');

        if ($root == self::SELECTED_PAGES) {
            $pages = $pageManager->loadArray($this->getModuleParameter('selected_pages'), true);

        } else {
            switch ($root) {
                case self::CURRENT_PAGE_PARENT:
                    $rootId = current($this->getPage()->getParentIds());
                    break;

                case self::CURRENT_PAGE:
                    $rootId = $this->getPage()->getId();
                    break;

                default:
                    $rootId = $this->getModuleParameter('root_page');
                    break;
            }

            $pages = $pageManager->getTree(null, $rootId, true);
        }

        return $this->render('index', [
            'menu' => $this->buildMenu($pages, $this->getModuleParameter('depth', 1))
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function breadcrumbsAction(Request $request)
    {
        $pages = [];
        $page = $this->getPage()->getPage();

        do {
            $pages[] = $page;
        } while ($page = $page->getParent());

        $menu = $this->buildMenu(array_reverse($pages));

        if ($breadcrumb = $this->getExtraBreadcrumb()) {
            $menuSize = count($menu);

            if ($breadcrumb->isReplacePrevious() && count($menu) > 1) {
                $menu[$menuSize - 1] = $breadcrumb;

            } else {
                $menu[] = $breadcrumb;
            }
        }

        return $this->render('breadcrumbs', ['menu' => $menu]);
    }

    /**
     * @return BreadcrumbMenuItem|null
     */
    protected function getExtraBreadcrumb()
    {
        $breadcrumb = $this->getPage()->getExtra('breadcrumb');

        if ($breadcrumb instanceof BreadcrumbMenuItem) {
            return $breadcrumb;
        }

        return null;
    }

    /**
     * @param Page[] $pages
     * @param int $depth
     * @return MenuItem[]
     */
    protected function buildMenu(array $pages, $depth = 1)
    {
        if ($depth < 1) return [];

        $menu = [];

        $currentPage = $this->getPage();

        foreach ($pages as $page) {
            $active = $page->getId() == $currentPage->getId();
            $parentActive = in_array($page->getId(), $currentPage->getParentIds());
            $sameSite = $page->getSite()->getId() == $currentPage->getSiteId();

            $menuItem = new MenuItem($page->getName(), $page->getUrl(!$sameSite));
            $menuItem
                ->setActive($active)
                ->setEnabled($page->isPublished())
                ->setParentActive($parentActive);

            $pageType = $page->getType();
            $menuItem->addExtra('pageType', $pageType);

            if ($pageType == Page::PAGE_TYPE_LINK) {
                $menuItem->addExtra('pageLinkType', $page->getLinkType());
            }

            /** @var ArrayCollection|Meta[] $hiddenMetas */
            $hiddenMetas = $page->getMetas()->filter(function(Meta $meta) {
                return $meta->getName()[0] == '-';
            });

            foreach ($hiddenMetas as $meta) {
                $menuItem->addExtra(substr($meta->getName(), 1), $meta->getContent());
            }

            $menuItem->setChildren($this->buildMenu($page->getChildren()->getValues(), $depth - 1));
            $menu[] = $menuItem;
        }

        return $menu;
    }
}
