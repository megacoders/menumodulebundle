<?php

namespace Megacoders\MenuModuleBundle\Model;


class BreadcrumbMenuItem extends MenuItem
{
    /**
     * @var bool
     */
    private $replacePrevious = false;

    /**
     * @return bool
     */
    public function isReplacePrevious()
    {
        return $this->replacePrevious;
    }

    /**
     * @param bool $replacePrevious
     * @return MenuItem
     */
    public function setReplacePrevious($replacePrevious)
    {
        $this->replacePrevious = $replacePrevious;
        return $this;
    }
}
