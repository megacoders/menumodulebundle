<?php

namespace Megacoders\MenuModuleBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;

class MenuItem
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var bool
     */
    private $active = false;

    /**
     * @var bool
     */
    private $enabled = true;

    /**
     * @var bool
     */
    private $parentActive = false;

    /**
     * @var MenuItem[]
     */
    private $children = [];

    /**
     * @var array
     */
    private $extras = [];

    /**
     * MenuItem constructor.
     * @param string $name
     * @param string $url
     */
    public function __construct($name, $url)
    {
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MenuItem
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return MenuItem
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return MenuItem
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return MenuItem
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isParentActive()
    {
        return $this->parentActive;
    }

    /**
     * @param bool $parentActive
     * @return MenuItem
     */
    public function setParentActive($parentActive)
    {
        $this->parentActive = $parentActive;
        return $this;
    }

    /**
     * @return array
     */
    public function getExtras()
    {
        return $this->extras;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param MenuItem[] $children
     * @return MenuItem
     */
    public function setChildren(array $children)
    {
        $this->children = [];

        foreach ($children as $child) {
            $this->addChild($child);
        }

        return $this;
    }

    /**
     * @param MenuItem $child
     * @return MenuItem
     */
    public function addChild(MenuItem $child)
    {
        $this->children[] = $child;
        return $this;
    }

    /**
     * @param MenuItem $child
     * @return MenuItem
     */
    public function removeChild(MenuItem $child)
    {
        $index = array_search($child, $this->children);

        if ($index !== false) {
            array_splice($this->children, $index, 1);
        }

        return $this;
    }

    /**
     * @param array $extras
     * @return MenuItem
     */
    public function setExtras($extras)
    {
        $this->extras = $extras;
        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addExtra($key, $value)
    {
        $this->extras[$key] = $value;
    }

}
